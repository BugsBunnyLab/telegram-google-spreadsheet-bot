
from enum import Enum

class API_Platform(Enum):
  telegram = 'telegram'
  google = 'google'
